import sys
import os.path
from pprint import pprint

def problem1(lines, logging=False) :
    daycount = 80
    fishes = list(map(int, lines[0].split(",")))

    for day in range(daycount):
        for i in range(len(fishes)):
            if fishes[i] == 0:
                fishes.append(8)
                fishes[i] = 6
            else:
                fishes[i] -= 1

    logPrint(fishes, logging)
    return len(fishes)

def problem2(lines, logging=False) :
    daycount = 256
    fishes = list(map(int, lines[0].split(",")))
    fishes.sort()

    fishCounts = {0: 0, 1: 0, 2: 0, 3:0, 4:0, 5:0, 6:0, 7:0, 8: 0}
    for fish in fishes:
        fishCounts[fish] = fishCounts[fish] + 1

    for day in range(daycount):
        last = fishCounts[8]
        for i in range(7, -1, -1):
            if i == 0:
                fishCounts[8] = fishCounts[0]
                fishCounts[6] = fishCounts[6] + fishCounts[0]
                fishCounts[0] = last
            else:
                temp = fishCounts[i]
                fishCounts[i] = last
                last = temp

    logPrint(fishCounts, logging)
    return sum(fishCounts.values())

def logPrint(value, logging=False): 
    if logging: print(value)

def main():
    
    FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines, "-d1" in sys.argv)))
    print("Problem 2 solution: " + str(problem2(lines, "-d2" in sys.argv)))

if __name__ == "__main__":
    main()

