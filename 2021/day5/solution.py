import re
import sys
import numpy
from pprint import pprint

def problem1(lines, logging=False) :
    ventedPoints = {}
    for index, line in enumerate(lines):
        parts = re.split("[, \n]", line)
        x1, y1, x2, y2 = 0,0,0,0
        if int(parts[0]) <= int(parts[3]):
            x1, y1, x2, y2 = int(parts[0]), int(parts[1]), int(parts[3]), int(parts[4])
        else:
            x1, y1, x2, y2 = int(parts[3]), int(parts[4]), int(parts[0]), int(parts[1]) 

        if y1 == y2:
            for i in range(x1, x2 + 1):
                if ventedPoints.get((i, y1)) == None:
                    ventedPoints[(i, y1)] = 1
                else:
                    ventedPoints[(i, y1)] += 1
        elif x1 == x2:
            for i in range(numpy.min([y1, y2]), numpy.max([y1, y2]) + 1):
                if ventedPoints.get((x1, i)) == None:
                    ventedPoints[(x1, i)] = 1
                else:
                    ventedPoints[(x1, i)] += 1


    crosses = 0
    for n in ventedPoints.values():
        if n > 1: crosses += 1

    logPrint(ventedPoints, logging)
    return crosses

def problem2(lines, logging=False) :
    ventedPoints = {}
    for index, line in enumerate(lines):
        parts = re.split("[, \n]", line)
        x1, y1, x2, y2 = 0,0,0,0
        if int(parts[0]) <= int(parts[3]):
            x1, y1, x2, y2 = int(parts[0]), int(parts[1]), int(parts[3]), int(parts[4])
        else:
            x1, y1, x2, y2 = int(parts[3]), int(parts[4]), int(parts[0]), int(parts[1]) 

        if y1 == y2:
            for i in range(x1, x2 + 1):
                if ventedPoints.get((i, y1)) == None:
                    ventedPoints[(i, y1)] = 1
                else:
                    ventedPoints[(i, y1)] += 1
        elif x1 == x2:
            for i in range(numpy.min([y1, y2]), numpy.max([y1, y2]) + 1):
                if ventedPoints.get((x1, i)) == None:
                    ventedPoints[(x1, i)] = 1
                else:
                    ventedPoints[(x1, i)] += 1
        else:
            for i in range(x2 - x1 + 1):
                yDir = 1 if y2 > y1 else -1
                if ventedPoints.get((x1 + i, y1 + i * yDir)) == None:
                    ventedPoints[(x1 + i, y1 + i * yDir)] = 1
                else:
                    ventedPoints[(x1 + i, y1 + i * yDir)] += 1


    crosses = 0
    for n in ventedPoints.values():
        if n > 1: crosses += 1

    logPrint(ventedPoints, logging)
    return crosses

def logPrint(value, logging=False): 
    if logging: print(value)

def main():
    FILENAME = "sample.txt" if "-t" in sys.argv else "input.txt"

    lines = []
    print(FILENAME)
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()

