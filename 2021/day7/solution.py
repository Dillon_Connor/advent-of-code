import numpy
import math
import sys
import os.path

def problem1(lines, logging=False) :
    positions = list(map(int, lines[0].split(",")))
    positions.sort()

    return numpy.sum(list(map(lambda n: abs(n-int(numpy.median(positions))), positions)))

def problem2(lines, logging=False) :
    positions = list(map(int, lines[0].split(",")))

    floor = math.floor(numpy.mean(positions))
    ceiling = math.ceil(numpy.mean(positions))
    return int(
            numpy.min([numpy.sum(list(map(lambda n: numpy.sum(range(1, abs(n-int(floor)) + 1)), positions))),
            numpy.sum(list(map(lambda n: numpy.sum(range(1, abs(n-int(ceiling)) + 1)), positions)))])
        )

def logPrint(value, logging=False): 
    if logging: print(value)

def main():
    
    FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines, "-d1" in sys.argv)))
    print("Problem 2 solution: " + str(problem2(lines, "-d2" in sys.argv)))

if __name__ == "__main__":
    main()
