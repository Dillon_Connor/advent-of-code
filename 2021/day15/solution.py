import sys
import re
import math
from collections import deque
import pprint
import os.path

def problem1(lines):
    return get_escape_cost(lines)

def problem2(lines):
    new_lines = increment_square(lines, 5)

    #pprint.pprint(new_lines)
    return get_escape_cost(new_lines)


def get_escape_cost(cave_string):
    cave = dict()
    costs = dict()
    unvisited = deque()
    for i, line in enumerate(cave_string):
        for j, c in enumerate(line.strip()):
            cave[(i, j)] = int(c)
            costs[(i, j)] = -1
            unvisited.append((i, j))

    size = math.sqrt(len(cave))
    cur = (0, 0)
    costs[cur] = 0
    unvisited.popleft()
    uset = set(unvisited)
    iterations = 0
    while len(unvisited) > 0:
        iterations += 1
        for neighbor in get_neighbors(cur, size):
            if (costs[neighbor] == -1
                or costs[neighbor] > costs[cur] + cave[neighbor]):

                costs[neighbor] = costs[cur] + cave[neighbor]
                if neighbor not in uset:
                    unvisited.appendleft(neighbor)
                    uset.add(neighbor)
        cur = unvisited.popleft()
        uset.remove(cur)

    return costs[(size - 1, size - 1)]

def get_neighbors(p, size):
    pot_neighbors = [(p[0]+1, p[1]), (p[0]-1, p[1]),
                     (p[0], p[1]+1), (p[0], p[1]-1)]

    return [p for p in pot_neighbors if p[0] > -1 and p[0] < size
            and p[1] > -1 and p[1] < size]

def increment_square(square, exf):
    new_sq = []
    ex_count = len(square) * exf
    for i in range(ex_count):
        new_sq.append("")
        rep_line = square[i % len(square)].strip()
        vv = i // len(square)

        for j in range(ex_count):
            c = rep_line[j % len(rep_line)]
            hv = j // len(square)
            new_val = int(c) + hv + vv
            str_val = str(new_val % 10 + 1) if new_val > 9 else str(new_val)
            new_sq[i] += str_val
    return new_sq

def main():
    filename = "input.txt"
    for argv in sys.argv:
        if re.search("-t([0-9]+)?", argv):
            filename = "sample" + argv.split("-t")[1] + ".txt"

    full_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), filename)

    lines = []
    with open(full_path) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
