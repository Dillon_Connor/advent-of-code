import re
import numpy
from pprint import pprint

TEST = False
FILENAME = "sample.txt" if TEST else "input.txt"
COLUMNS = 5
ROWS = 5

lines = []
with open(FILENAME) as f:
    lines = f.readlines()

def problem1(logging=False) :
    numbers = lines[0].split(",")
    boards = setBoards()


    winningBoardId = None
    winningMoves = None
    winningNumber = None
    winningBoardScore = 0
    winningBoardString = None
    for index, board in enumerate(boards):
        hitCount = 0
        for i in range(len(numbers)):
            number = int(numbers[i])
            for j in range(COLUMNS * ROWS):
                if board[int(j / COLUMNS)][j % COLUMNS] == number:
                    board[int(j / COLUMNS)][j % COLUMNS] = -1
                    hitCount += 1
                    continue

            rowTotals = numpy.sum(board, axis=1)
            columnTotals = numpy.sum(board, axis=0)

            if -5 in rowTotals or -5 in columnTotals:
                    if winningMoves == None or i < winningMoves:
                        winningMoves = i
                        winningBoardScore = (numpy.sum(board) + hitCount) * number
                        winningBoardId = index
                        winningNumber = number
                        winningBoardString = "Sum " + str(numpy.sum(boards[winningBoardId]) + winningMoves + 1) + " * " + str(winningNumber) + " = " + str(winningBoardScore)

                    boardComplete = True
                    break

    if logging:
        print("\nWinning Board: " + str(winningBoardId) + " with " + str(winningMoves) + " moves on " + str(winningNumber))
        pprint(boards[winningBoardId])
        print(winningBoardString)
    return winningBoardScore

def problem2(logging=False) :
    numbers = lines[0].split(",")
    boards = setBoards()


    winningBoardId = None
    winningMoves = None
    winningNumber = None
    winningBoardScore = 0
    winningBoardString = None
    for index, board in enumerate(boards):
        hitCount = 0
        for i in range(len(numbers)):
            number = int(numbers[i])
            for j in range(COLUMNS * ROWS):
                if board[int(j / COLUMNS)][j % COLUMNS] == number:
                    board[int(j / COLUMNS)][j % COLUMNS] = -1
                    hitCount += 1
                    continue

            rowTotals = numpy.sum(board, axis=1)
            columnTotals = numpy.sum(board, axis=0)

            if -5 in rowTotals or -5 in columnTotals:
                    if winningMoves == None or i > winningMoves:
                        winningMoves = i
                        winningBoardScore = (numpy.sum(board) + hitCount) * number
                        winningBoardId = index
                        winningNumber = number
                        winningBoardString = "Sum " + str(numpy.sum(boards[winningBoardId]) + winningMoves + 1) + " * " + str(winningNumber) + " = " + str(winningBoardScore)

                    boardComplete = True
                    break

    if logging:
        print("\nWinning Board: " + str(winningBoardId) + " with " + str(winningMoves) + " moves on " + str(winningNumber))
        pprint(boards[winningBoardId])
        print(winningBoardString)
    return winningBoardScore

def setBoards(): 
    boards = [[]]
    boardId = 0
    row = 0
    for i in range(2, len(lines)):
        line = lines[i]

        if len(line) == 1:
            boardId += 1
            row = 0
            boards.append([])
            continue

        boards[boardId].append([])

        numbers = line.split(" ")
        for j in range(len(numbers)):
            if len(numbers[j].strip()) == 0: continue
            boards[boardId][row].append(int(numbers[j].strip()))

        row += 1

    return boards

def main(runTest=False):
    print("Problem 1 solution: " + str(problem1()))
    print("Problem 2 solution: " + str(problem2(True)))

if __name__ == "__main__":
    main()

