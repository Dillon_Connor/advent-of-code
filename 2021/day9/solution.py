import sys
import os.path

def problem1(lines) :
    cave = []
    for i, line in enumerate(lines):
        cave.append([])
        for seg in list(line.strip()):
            cave[i].append(int(seg))

    dangerval = 0
    for i, row in enumerate(cave):
        for j, val in enumerate(row):
           if ((i-1 == -1 or val < cave[i-1][j])
           and (i+1 == len(cave) or val < cave[i+1][j])
           and (j-1 == -1 or val < cave[i][j-1])
           and (j+1 == len(row) or val < cave[i][j+1])):
               dangerval += 1 + val

    return dangerval

def problem2(lines) :
    cave = []
    for i, line in enumerate(lines):
        cave.append([])
        for seg in list(line.strip()):
            cave[i].append(int(seg))

    lows = dict()
    for i, row in enumerate(cave):
        for j, val in enumerate(row):
           if ((i-1 == -1 or val < cave[i-1][j])
           and (i+1 == len(cave) or val < cave[i+1][j])
           and (j-1 == -1 or val < cave[i][j-1])
           and (j+1 == len(row) or val < cave[i][j+1])):
               lows[(i, j)] = val

    basin_sizes = []
    for k, v in lows.items():
        basin_sizes.append(basin_iter(k, [], cave))
    
    basin_sizes.sort(reverse=True)
    return basin_sizes[0] * basin_sizes[1] * basin_sizes[2]

def basin_iter(point, visited, cave):
    if point in visited: return 0
    visited.append(point)

    if point[0] == -1 or point[0] == len(cave) or point[1] == -1 or point[1] == len(cave[0]) or cave[point[0]][point[1]] == 9:
        return 0
    else:
        return (
                1
                + basin_iter((point[0] - 1, point[1]), visited, cave)
                + basin_iter((point[0] + 1, point[1]), visited, cave)
                + basin_iter((point[0], point[1] - 1), visited, cave)
                + basin_iter((point[0], point[1] + 1), visited, cave)
                )

def main():
    
    FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
