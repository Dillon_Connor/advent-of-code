import sys
import pprint
import os.path

def problem1(lines) :
    octopi = []
    for i, line in enumerate(lines):
        octopi.append([])
        for e in line.strip():
            octopi[i].append(int(e));

    flashes = 0
    for i in range(100):
        flashes += complete_step(octopi)
    return flashes

def problem2(lines) :
    octopi = []
    for i, line in enumerate(lines):
        octopi.append([])
        for e in line.strip():
            octopi[i].append(int(e));

    i = 0
    while True:
        i += 1
        if complete_step(octopi) == len(octopi) * len(octopi[0]):
            return i

def complete_step(octopi):
    flash_count = 0
    for i, o in enumerate(octopi):
        for j, e in enumerate(octopi[i]):
            octopi[i][j] += 1

    flashed = True
    flashes = dict()
    while flashed:
        flashed = False
        for i, o in enumerate(octopi):
            for j, e in enumerate(octopi[i]):
                if octopi[i][j] > 9:
                    flash_count += 1
                    flashes[(i, j)] = True
                    octopi[i][j] = 0

                    update_adjacent(octopi, i , j, flashes)
                    flashed = True

    return flash_count

def update_adjacent(octopi, i, j, flashes):
    if i - 1 > -1 and j - 1 > -1 and (i-1, j-1) not in flashes:
        octopi[i - 1][j - 1] += 1

    if i+1 < len(octopi) and j+1 < len(octopi[0]) and (i+1, j+1) not in flashes:
        octopi[i + 1][j + 1] += 1

    if i - 1 > -1 and j + 1 < len(octopi[0]) and (i-1, j+1) not in flashes:
        octopi[i - 1][j + 1] += 1

    if i + 1 < len(octopi) and j - 1 > -1 and (i+1, j-1) not in flashes:
        octopi[i + 1][j - 1] += 1

    if i - 1 > -1 and (i-1, j) not in flashes:
        octopi[i - 1][j] += 1

    if i + 1 < len(octopi) and (i+1, j) not in flashes:
        octopi[i + 1][j] += 1

    if j - 1 > -1 and (i, j-1) not in flashes:
        octopi[i][j - 1] += 1

    if j + 1 < len(octopi[0]) and (i, j+1) not in flashes:
        octopi[i][j + 1] += 1

    return


def main():
    FILENAME = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
