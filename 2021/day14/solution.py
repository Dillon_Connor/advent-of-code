import sys
import re
import pprint
import os.path

def problem1(lines):
    ptemplate = ""
    poly_pairs = dict()
    for line in lines:
        if line == "\n": continue
        if ptemplate == "":
            ptemplate = line.strip()
            continue
        parts = line.strip().split(" -> ")
        poly_pairs[parts[0]] = parts[1]

    step_count = 10
    grown_polymer = ptemplate
    for i in range(step_count):
        growth = grown_polymer[0]
        for j in range(len(grown_polymer) - 1):
            pair = grown_polymer[j] + grown_polymer[j + 1]
            if pair in poly_pairs:
                growth += poly_pairs[pair] + grown_polymer[j + 1]
            else:
                growth += grown_polymer[j + 1]
        grown_polymer = growth

    polycount = dict()
    for c in grown_polymer:
        if c not in polycount:
            polycount[c] = 0
        polycount[c] += 1

    max_polycount, min_polycount = 0, 0
    for c in polycount.values():
        if max_polycount == 0 or c > max_polycount:
            max_polycount = c
        if min_polycount == 0 or c < min_polycount:
            min_polycount = c

    return max_polycount - min_polycount

def problem2(lines):
    ptemplate = ""
    poly_pairs = dict()
    for line in lines:
        if line == "\n": continue
        if ptemplate == "":
            ptemplate = line.strip()
            continue
        parts = line.strip().split(" -> ")
        poly_pairs[parts[0]] = parts[1]

    grown_pairs = {ptemplate[len(ptemplate) - 1]: 1}
    for c in range(len(ptemplate) - 1):
        pair = ptemplate[c] + ptemplate[c + 1]
        if pair not in grown_pairs:
            grown_pairs[pair] = 0
        grown_pairs[pair] += 1

    step_count = 40
    for i in range(step_count):
        growth = grown_pairs.copy()
        for pair in grown_pairs.keys():
            if pair in poly_pairs:
                factor = grown_pairs[pair]

                left_pair = pair[0] + poly_pairs[pair]
                if left_pair not in growth:
                    growth[left_pair] = 0

                right_pair = poly_pairs[pair] + pair[1]
                if right_pair not in growth:
                    growth[right_pair] = 0

                growth[left_pair] += factor
                growth[right_pair] += factor
                growth[pair] -= factor
            if growth[pair] == 0:
                del growth[pair]
        grown_pairs = growth

    polycount = dict()
    for k, v in grown_pairs.items():
        if k[0] not in polycount:
            polycount[k[0]] = 0
        polycount[k[0]] += v

    return max(polycount.values()) - min(polycount.values())

def main():
    filename = "input.txt"
    for argv in sys.argv:
        if re.search("-t([0-9]+)?", argv):
            filename = "sample" + argv.split("-t")[1] + ".txt"

    full_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), filename)

    lines = []
    with open(full_path) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
