import sys
import re
import math
from collections import deque
import pprint
import os.path

def problem1(lines):
    set_maps()
    inbound_packet = lines[0].strip()
    exp_packet = "".join([DMAP[c] for c in inbound_packet])

    version_sum = 0
    packets = deque([exp_packet])
    while len(packets) > 0:
        packet = packets.pop()
        version = int(packet[0:3], 2)
        ptype = packet[3:6]
        version_sum += version
        if ptype == "100":
            pass
        else:
            length_type = packet[6]
            if length_type == "0":
                new_packets = get_packets_by_length(packet)
            else:
                new_packets = get_packets_by_count(packet)
            packets.extend(new_packets)
    return version_sum

def problem2(lines):
    set_maps()
    inbound_packet = lines[0].strip()
    exp_packet = "".join([DMAP[c] for c in inbound_packet])

    parent = Packet(exp_packet)
    packets = deque([parent])
    while len(packets) > 0:
        packet = packets.pop()
        if packet.ptype != "100":
            if packet.length_type == "0":
                new_packets = [Packet(p) for p in get_packets_by_length(packet.string)]
            else:
                new_packets = [Packet(p) for p in get_packets_by_count(packet.string)]
            packet.packets.extend(new_packets)
            packets.extend(new_packets)
    return parent.get_value()

class Packet:
    def __init__(self, string):
        self.string = string
        self.packets = deque()

        self.version = int(string[0:3], 2)
        self.ptype = string[3:6]
        self.length_type = string[6]
        self.op_type = string[3:6]
    
    def get_value(self):
        if self.ptype == "100":
            pb_str = ""
            seg_start = 6
            while True:
                g = self.string[seg_start]
                pb_str += self.string[seg_start + 1:seg_start+5]
                seg_start += 5
                if g == "0": break
            return int(pb_str, 2)
        else:
            literals = []
            for p in self.packets:
                literals.append(p.get_value())
            
            if self.op_type == "000":
                return sum(literals)
            elif self.op_type == "001":
                return math.prod(literals)
            elif self.op_type == "010":
                return min(literals)
            elif self.op_type == "011":
                return max(literals)
            elif self.op_type == "101":
                return 1 if literals[1] < literals[0] else 0
            elif self.op_type == "110":
                return 1 if literals[1] > literals[0] else 0
            elif self.op_type == "111":
                return 1 if literals[1] == literals[0] else 0

            

def get_packets_by_length(packets_string):
    length = 22 + int(packets_string[7:22], 2)
    packets = []
    seg_start = 22
    scount = 0
    while True:
        if packets_string[seg_start+3:seg_start+6] == "100":
            scount = get_literal_length(packets_string[seg_start:])
        else:
            scount = get_operator_length(packets_string[seg_start:])

        packets.append(packets_string[seg_start:seg_start + scount])
        seg_start += scount

        if "1" not in packets_string[seg_start:] or seg_start > length: break
    return packets

def get_packets_by_count(packets_string):
    count = int(packets_string[7:18], 2)
    packets = []
    seg_start = 18
    scount = 0
    while True:
        if packets_string[seg_start+3:seg_start+6] == "100":
            scount = get_literal_length(packets_string[seg_start:])
        else:
            scount = get_operator_length(packets_string[seg_start:])

        packets.append(packets_string[seg_start:seg_start + scount])
        if len(packets) == count:
            return packets

        seg_start += scount
    return packets

def get_literal_length(lstr):
    seg_start = 6
    while True:
        g = lstr[seg_start]
        seg_start += 5
        if g == "0": break
    return seg_start

def get_operator_length(ostr):
    if ostr[6] == "0":
        return 22 + int(ostr[7:22], 2)
        
    count = int(ostr[7:18], 2)
    seg_start = 18
    for i in range(count):
        if ostr[seg_start+3:seg_start+6] == "100":
            seg_start += get_literal_length(ostr[seg_start:])
        else:
            seg_start += get_operator_length(ostr[seg_start:]) 
    return seg_start
    

def calculate(operator, literals):
    operator_type = operator[3:6]
    if operator_type == "000":
        return sum(literals)
    elif operator_type == "001":
        return math.prod(literals)
    elif operator_type == "010":
        return min(literals)
    elif operator_type == "011":
        return max(literals)
    elif operator_type == "101":
        return 1 if literals[1] > literals[0] else 0
    elif operator_type == "110":
        return 1 if literals[1] < literals[0] else 0
    elif operator_type == "111":
        return 1 if literals[1] == literals[0] else 0

def set_maps():
    global DMAP, BMAP
    DMAP = {"0": "0000",
            "1": "0001",
            "2": "0010",
            "3": "0011",
            "4": "0100",
            "5": "0101",
            "6": "0110",
            "7": "0111",
            "8": "1000",
            "9": "1001",
            "A": "1010",
            "B": "1011",
            "C": "1100",
            "D": "1101",
            "E": "1110",
            "F": "1111"}

    BMAP = {v: k for k, v in DMAP.items()}

def main():
    filename = "input.txt"
    for argv in sys.argv:
        if re.search("-t([0-9]+)?", argv):
            filename = "sample" + argv.split("-t")[1] + ".txt"

    full_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), filename)

    lines = []
    with open(full_path) as f:
        lines = f.readlines()

    #print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
