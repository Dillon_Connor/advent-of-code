import sys
import re
import pprint
import os.path

def problem1(lines):
    points = set()
    instructions = []

    onpoints = True
    for line in lines:
        if line == "\n":
            onpoints = False
            continue

        if onpoints:
            split = line.strip().split(",")
            points.add((int(split[0]), int(split[1])))
        else:
            instructions.append((line[11], int(line.strip().split("=")[1])))

    instruction = instructions[0]
    point_copy = points.copy()
    for point in point_copy:
        value = instruction[1]
        if instruction[0] == "x":
            if point[0] > value:
                points.remove(point)
                points.add((value - (point[0] - value), point[1]))
        else:
            if point[1] > value:
                points.remove(point)
                points.add((point[0], value - (point[1] - value)))

    return len(points)

def problem2(lines):
    points = set()
    instructions = []

    onpoints = True
    for line in lines:
        if line == "\n":
            onpoints = False
            continue

        if onpoints:
            split = line.strip().split(",")
            points.add((int(split[0]), int(split[1])))
        else:
            instructions.append((line[11], int(line.strip().split("=")[1])))

    for instruction in instructions:
        point_copy = points.copy()
        for point in point_copy:
            value = instruction[1]
            if instruction[0] == "x":
                if point[0] > value:
                    points.remove(point)
                    points.add((value - (point[0] - value), point[1]))
            else:
                if point[1] > value:
                    points.remove(point)
                    points.add((point[0], value - (point[1] - value)))
        point_copy = points

    x_max, y_max = 0, 0
    for point in points:
        if point[0] > x_max: x_max = point[0]
        if point[1] > y_max: y_max = point[1]

    output = "\n"
    for y in range(y_max + 1):
        for x in range(x_max + 1):
            if (x, y) in points: output += "\u2588"
            else: output += " "
        output += "\n"

    return output

def main():
    filename = "input.txt"
    for argv in sys.argv:
        if re.search("-t([0-9]+)?", argv):
            filename = "sample" + argv.split("-t")[1] + ".txt"

    full_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), filename)

    lines = []
    with open(full_path) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
