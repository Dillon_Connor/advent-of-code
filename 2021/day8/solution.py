import sys
import os.path

def problem1(lines) :
    outputs = []
    for line in lines:
        outputs.extend(list(map(lambda x: x.strip(), line.split(" | ")[1].split(" "))))
        
    return len([x for x in outputs if len(x) < 5 or len(x) > 6])

def problem2(lines, logging=False) :
    total = 0
    for line in lines:
        answer = ""
        allnums = list(map(lambda x: x.strip(), line.split(" | ")[0].split(" ")))

        knownnumbers = dict()
        knownnumbers["1"] = get_matching_len(allnums, 2)[0]
        knownnumbers["7"] = get_matching_len(allnums, 3)[0]
        knownnumbers["4"] = get_matching_len(allnums, 4)[0]
        knownnumbers["8"] = get_matching_len(allnums, 7)[0]
        knownnumbers["6"] = get_by_segment_count(get_matching_len(allnums, 6), knownnumbers["1"], 1)[0]
        knownnumbers["9"] = get_by_segment_count(get_matching_len(allnums, 6), knownnumbers["4"], 4)[0]
        knownnumbers["3"] = get_by_segment_count(get_matching_len(allnums, 5), knownnumbers["7"], 3)[0]
        knownnumbers["2"] = get_by_segment_count(get_matching_len(allnums, 5), knownnumbers["4"], 2)[0]
        knownnumbers["5"] = get_by_segment_count(get_matching_len(allnums, 5), knownnumbers["2"], 3)[0]
        knownnumbers["0"] = get_by_segment_count(get_matching_len(allnums, 6), knownnumbers["5"], 4)[0]

        orderedkeys = dict()
        for k, v in knownnumbers.items():
            orderedkeys["".join(sorted(v))] = k

        for v in list(map(lambda x: x.strip(), line.split(" | ")[1].split(" "))):
            answer += orderedkeys["".join(sorted(v))]

        total += int(answer)

    return total

def get_by_segment_count(sourcesig, comsig, segcount):
    return [x for x in sourcesig if match_segment_count(x, comsig, segcount)]

def get_matching_len(allnums, count):
   return [x for x in allnums if len(x) == count]

def match_segment_count(sourcesig, comsig, segcount):
   return len([x for x in list(sourcesig) if x in comsig]) == segcount 

def main():
    
    FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
