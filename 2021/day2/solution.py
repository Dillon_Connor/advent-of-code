import re

lines = []
with open("input.txt") as f:
    lines = f.readlines()

def problem1(logging=False) :
    horizontal = 0
    depth = 0
    for i in range(len(lines)):
        reg = re.compile("([a-z]+)(\s)(\d+)")
        command = reg.match(lines[i]).groups()[0]
        value = int(reg.match(lines[i]).groups()[2])

        if command == "forward":
            horizontal += value
        elif command == "down":
            depth += value
        elif command == "up":
            depth -= value

    if logging: print("horizontal: " + str(horizontal))
    if logging: print("depth: " + str(depth))
    return horizontal * depth

def problem2(logging=False) :
    horizontal = 0
    depth = 0
    aim = 0
    for i in range(len(lines)):
        reg = re.compile("([a-z]+)(\s)(\d+)")
        command = reg.match(lines[i]).groups()[0]
        value = int(reg.match(lines[i]).groups()[2])

        if command == "forward":
            horizontal += value
            depth += value * aim
        elif command == "down":
            aim += value
        elif command == "up":
            aim -= value

    if logging: print("horizontal: " + str(horizontal))
    if logging: print("depth: " + str(depth))
    return horizontal * depth

print("Problem 1 solution: " + str(problem1()))
print("Problem 2 solution: " + str(problem2()))
