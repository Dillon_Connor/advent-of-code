lines = []
with open("input.txt") as f:
	lines = f.readlines()

def problem1() :
	count = 0
	for i in range(len(lines) - 1):
		if int(lines[i]) < int(lines[i+1]):
			count = count + 1
	return count

def problem2() :
	count = 0
	for i in range(1, len(lines) - 2):
		if int(lines[i-1]) + int(lines[i]) + int(lines[i+1]) < int(lines[i]) + int(lines[i+1]) + int(lines[i+2]):
			count = count + 1
	return count

print("Problem 1 solution: " + str(problem1()))
print("Problem 2 solution: " + str(problem2()))
