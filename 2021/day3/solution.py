import copy

lines = []
with open("input.txt") as f:
    lines = f.readlines()

def problem1(logging=False) :
    zerosCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    onesCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(lines)):
        for j in range(len(zerosCount) - 1):
            if lines[i][j] == "0":
                zerosCount[j] += 1
            else:
                onesCount[j] += 1

    gamma = ""
    epsilon = ""

    for i in range(len(zerosCount) - 1):
        if zerosCount[i] > onesCount[i]:
            gamma = gamma + "0"
            epsilon = epsilon + "1"
        else:
            gamma = gamma + "1"
            epsilon = epsilon + "0"

    if logging: print("gamma: " + gamma + " = " + str(int(gamma, 2)))
    if logging: print("epsilon: " + epsilon + " = " + str(int(epsilon, 2)))
    return int(gamma, 2) * int(epsilon, 2)

def problem2(logging=False) :
    f = 0
    oxygenList = copy.deepcopy(lines)
    while len(oxygenList) > 1 and f < 12:
        oneCount, zeroCount = 0, 0
        for i in range(len(oxygenList) - 1, -1, -1):
            if oxygenList[i][f] == "0":
                zeroCount += 1
            else:
                oneCount +=1
        
        comparison = "1" if oneCount >= zeroCount else "0"
        for i in range(len(oxygenList) - 1, -1, -1):
            if oxygenList[i][f] != comparison:
                del oxygenList[i]
        
        f += 1

    f = 0
    co2List = copy.deepcopy(lines)
    while len(co2List) > 1 and f < 12:
        oneCount, zeroCount = 0, 0
        for i in range(len(co2List) - 1, -1, -1):
            if co2List[i][f] == "0":
                zeroCount += 1
            else:
                oneCount +=1
        
        comparison = "1" if oneCount < zeroCount else "0"
        for i in range(len(co2List) - 1, -1, -1):
            if co2List[i][f] != comparison:
                del co2List[i]
        
        f += 1

    if logging: print("oxygenList: " + oxygenList[0] + " = " + str(int(oxygenList[0], 2)))
    if logging: print("co2List: " + co2List[0] + " + " + str(int(co2List[0], 2)))

    return int(oxygenList[0], 2) * int(co2List[0], 2)

print("Problem 1 solution: " + str(problem1()))
print("Problem 2 solution: " + str(problem2()))
