import sys
import os.path

groupings = {"(":")", "[":"]", "{":"}", "<":">"}
def problem1(lines) :
    scores = {")": 3, "]": 57, "}": 1197, ">": 25137}
    score = 0
    for line in lines:
        res = check_corrupt(line.strip())
        if res != False:
            score += scores[res]

    return score

def problem2(lines) :
    for i in range(len(lines) - 1, -1, -1):
        res = check_corrupt(lines[i].strip())
        if res != False:
            del lines[i]

    scores = []
    for line in lines:
        scores.append(finish_score(finish_line(line.strip())))

    return sorted(scores)[int(len(scores) / 2)]

def check_corrupt(line):
    for i, x in enumerate(line):
        if line[i] not in groupings.keys() or i + 1 > len(line) - 1 or line[i + 1] not in groupings.keys(): continue

        ocount = 0
        for j, char in enumerate(line[i + 1:]):
            if char in groupings.keys():
                ocount += 1
            else:
                ocount -= 1

            if ocount == -1:
                if char != groupings[line[i]]:
                    return char
                break
    return False

def finish_line(line):
    finished = ""
    for i, x in enumerate(line):
        if line[i] not in groupings.keys() or i + 1 > len(line): continue

        ocount = 0
        missing = True
        for j, char in enumerate(line[i + 1:]):
            if char in groupings.keys():
                ocount += 1
            else:
                ocount -= 1

            if ocount == -1:
                missing = False
                break

        if missing:
            finished = groupings[line[i]] + finished
    return finished

def finish_score(finished):
    scores = {")": 1, "]": 2, "}": 3, ">": 4}
    score = 0
    for char in finished:
        score = 5 * score + scores[char]

    return score

def main():
    
    FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
