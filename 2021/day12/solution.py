import sys
import re
import pprint
import os.path

def problem1(lines):
    connections = dict()
    for line in lines:
        parts = line.strip().split("-")
        if parts[0] not in connections:
            connections[parts[0]] = []

        idx = len(connections[parts[0]])
        if "end" in connections[parts[0]]:
            idx = len(connections[parts[0]]) - 1
        connections[parts[0]].insert(idx, parts[1])

        if parts[1] not in connections:
            connections[parts[1]] = []

        idx = len(connections[parts[1]])
        if "end" in connections[parts[1]]:
            idx = len(connections[parts[1]]) - 1
        connections[parts[1]].insert(idx, parts[0])

    paths = get_paths(connections, False)

    return len(paths)

def problem2(lines):
    connections = dict()
    for line in lines:
        parts = line.strip().split("-")
        if parts[0] not in connections:
            connections[parts[0]] = []

        idx = len(connections[parts[0]])
        if "end" in connections[parts[0]]:
            idx = len(connections[parts[0]]) - 1
        connections[parts[0]].insert(idx, parts[1])

        if parts[1] not in connections:
            connections[parts[1]] = []

        idx = len(connections[parts[1]])
        if "end" in connections[parts[1]]:
            idx = len(connections[parts[1]]) - 1
        connections[parts[1]].insert(idx, parts[0])

    paths = get_paths(connections, True)

    return len(paths)

def get_paths(connections, smalls):
    valid_paths = []
    path_stack = ["start"]
    count_stack = [0]
    near_caves = connections["start"]
    debug_counter = 0
    while len(path_stack) > 0:
        if count_stack[-1] == len(near_caves):
            path_stack.pop()
            count_stack.pop()
            if len(path_stack) == 0: break
            count_stack[-1] += 1
            near_caves = connections[path_stack[-1]]
            continue

        next_cave = near_caves[count_stack[-1]]
        show = {"path_stack": path_stack, "count_stack": count_stack,
                "near_caves": near_caves, "next_cave": next_cave}
#        pprint.pprint(show)
#        print()

        if next_cave == "end":
            copied_path = path_stack.copy()
            copied_path.append(next_cave)
            valid_paths.append(copied_path)

            path_stack.pop()
            count_stack.pop()
            count_stack[-1] += 1
            near_caves = connections[path_stack[-1]]
        elif next_cave == "start":
            count_stack[-1] += 1
        elif (next_cave.islower()
              and small_cave_violations(smalls, next_cave, path_stack)):
            count_stack[-1] += 1
        else:
            path_stack.append(next_cave)
            count_stack.append(0)
            near_caves = connections[next_cave]

    return valid_paths

def small_cave_violations(smalls, next_cave, path_stack):
    if smalls:
        full = False
        smcs = dict()
        for cave in path_stack:
            if cave != "start" and cave != "end" and cave.islower():
                if cave in smcs:
                    full = True
                    break
                else:
                    smcs[cave] = True
        return full and next_cave in path_stack
    return next_cave in path_stack

def end_key(s):
    return 1 if s == "end" else 0

def main():
    filename = "input.txt"
    for argv in sys.argv:
        if re.search("-t([0-9]+)?", argv):
            filename = "sample" + argv.split("-t")[1] + ".txt"

    full_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), filename)

    lines = []
    with open(full_path) as f:
        lines = f.readlines()

    print("Problem 1 solution: " + str(problem1(lines)))
    print("Problem 2 solution: " + str(problem2(lines)))

if __name__ == "__main__":
    main()
