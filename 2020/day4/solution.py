import sys
import re
import os.path
from pprint import pprint

TEST = False
def problem1(content):
    passports = re.split("\n\n", content.strip())
    validpasses = 0
    for passport_string in passports:
        passport = dict()
        for element in re.split("\n| ", passport_string):
            passport[re.split(":", element)[0]] = re.split(":", element)[1]

        required_keys = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        if all(key in passport.keys() for key in required_keys):
            validpasses += 1

    return validpasses

def problem2(content):
    passports = re.split("\n\n", content.strip())
    validpasses = 0
    for passport_string in passports:
        passport = dict()
        for element in re.split("\n| ", passport_string):
            if re.split(":", element)[0] == "cid": continue
            passport[re.split(":", element)[0]] = re.split(":", element)[1]

        if validate_passport(passport):
            validpasses += 1

    return validpasses

def validate_passport(passport):
    if ("byr" not in passport
        or not passport["byr"].isnumeric()
        or int(passport["byr"]) < 1920
        or int(passport["byr"]) > 2002):
        return False

    if ("iyr" not in passport
        or not passport["iyr"].isnumeric()
        or int(passport["iyr"]) < 2010
        or int(passport["iyr"]) > 2020):
        return False

    if ("eyr" not in passport
        or not passport["eyr"].isnumeric()
        or int(passport["eyr"]) < 2020
        or int(passport["eyr"]) > 2030):
        return False

    if ("hgt" not in passport
        or ("cm" not in passport["hgt"] and "in" not in passport["hgt"])
        or ("cm" in passport["hgt"]
            and (int(passport["hgt"].split("cm")[0]) < 150
                 or int(passport["hgt"].split("cm")[0]) > 193))
        or ("in" in passport["hgt"]
            and (int(passport["hgt"].split("in")[0]) < 59
                 or int(passport["hgt"].split("in")[0]) > 76))):
        return False

    if ("hcl" not in passport
        or not re.search("#[0-9a-f]{6}", passport["hcl"])):
        return False

    eyecolors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    if ("ecl" not in passport
        or passport["ecl"] not in eyecolors):
        return False

    if ("pid" not in passport
        or not re.search("[0-9]{9}", passport["pid"])):
        return False

    return True

def main():
    FILENAME = os.path.join(os.path.dirname(
        os.path.abspath(__file__)),
        "sample.txt" if "-t" in sys.argv or TEST else "input.txt")

    lines = []
    with open(FILENAME) as f:
        lines = f.readlines()

    content = open(FILENAME).read()

    print("Problem 1: " + str(problem1(content)))
    print("Problem 2: " + str(problem2(content)))

if __name__ == "__main__":
    main()

