import re
import sys
import numpy
import os.path
from pprint import pprint

def problem1(lines, logging=False) :
    x, trees = 0, 0
    for i, line in enumerate(lines):
        line.rstrip()
        if line[x] == "#":
            trees += 1

        x = (x + 3) % (len(line) - 1)

    return trees

def problem2(lines, logging=False) :
    instructions = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

    trees = 1;
    for instruction in instructions:
        trees *= getTreeCount(lines, instruction)

    return trees

def getTreeCount(lines, instruction):
    x, trees = 0, 0
    for i in range(0, len(lines), instruction[1]):
        line = lines[i]
        line.rstrip()
        if line[x] == "#":
            trees += 1

        x = (x + instruction[0]) % (len(line) - 1)

    return trees

def logPrint(value, logging=False): 
    if logging: print value

def main():
    
    FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample.txt" if "-t" in sys.argv else "input.txt")

    lines = []
    print FILENAME
    with open(FILENAME) as f:
        lines = f.readlines()

    print "Problem 1 solution: " + str(problem1(lines, "-d1" in sys.argv))
    print "Problem 2 solution: " + str(problem2(lines, "-d2" in sys.argv))

if __name__ == "__main__":
    main()

